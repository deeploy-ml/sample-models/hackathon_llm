#!/usr/bin/env bash
echo "Attempting Docker build and deploy"
read -p "Enter dockerhub user: " dockerhub




read -p "Enter dockerhub repo for transformer: [transformer_hackathon_llm] " transformer_repo
transformer_repo=${transformer_repo:-transformer_hackathon_llm}





echo "...... Building transformer ......"
docker build  . -f hackathon_llm_transformer/Dockerfile -t "$dockerhub"/"$transformer_repo":latest





echo "...... Pushing transformer artifact......"
docker push "$dockerhub"/"$transformer_repo":latest

