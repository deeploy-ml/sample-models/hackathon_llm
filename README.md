# Hackathon Challenge 2: LLMs
Welcome to the hackathon challenge 2!
In this repository you find a LLM which is configured to answer only bank related queries of user. However LLM models are far from perfect. Your task is to develop a pipeline that prevent irrelevant answers, adversarial attacks and any such unwarranted behaviour.

In order to do that you have to develop methods to catch irrelevant information in the pipeline.

## Building Transformer
To build interception utilize the cli of deeploy python client to build transformer capable of detecting irrelevant information. A sample transformer is provided to you for you better understanding. This transfomer adds a text at the end of response. 

Such a transformer can be made using our [Deeploy Python Client](https://pypi.org/project/deeploy/) CLI tool. More about it can be found [here](https://docs.deeploy.ml/python-client/cli).

## Making a model call
Please follow instructions in the docs to get deeploy API endpoint. Make the post request in the format as provided in **metadata.json**. 

NOTE: Make sure to only change prompt part. Other configurations should remain as it is for uniformity and standardized evaluation.

## Tips for deployment
Please follow the documentation in the docs page. The current model is a custom docker with blob so in order to deploy it please choose relevant options.

NOTE: Make sure to select max resources available to you. LLMs are compute hungry.

NOTE: Make sure that model is not serverless for faster inference.


