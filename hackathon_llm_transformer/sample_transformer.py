import logging
from typing import Dict

import kserve # To enable logging

from deeploy.cli.wrappers.transformer_wrapper import TransformerWrapper

## To enable logging
logging.basicConfig(level=kserve.constants.KSERVE_LOGLEVEL)

class SampleTransformer(TransformerWrapper):
    def __init__(
        self
    ):
        pass

    def _preprocess(self, payload: Dict) -> Dict:
        # Look up definitions in TransformerWrapper for more information on method.
        # NOTE: In case there is no transformation, return request
        
        ##We do nothing at preprocessing
        return payload
        
    def _postprocess(self, response: Dict) -> Dict:
        # Look up definitions in TransformerWrapper for more information on method.
        # NOTE: In case there is no transformation, return response

        ## We append I got transformed in response.
        response["response"] = response["response"] + "I got transformed!"
        return response